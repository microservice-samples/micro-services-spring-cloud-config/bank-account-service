package com.hendisantika.microservices.bankaccountservice.service;

import com.hendisantika.microservices.bankaccountservice.config.Configuration;
import com.hendisantika.microservices.bankaccountservice.exception.InvalidAccountBalanceException;
import com.hendisantika.microservices.bankaccountservice.model.AccountType;
import com.hendisantika.microservices.bankaccountservice.model.BankAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.36
 */
@Service
public class BankAccountService {

    private final Map<String, BankAccount> accountCache = new HashMap<>();
    @Autowired
    private Configuration configuration;

    @PostConstruct
    public void setupTestData() {

        BankAccount account1 = new BankAccount("A12345", "John Doe Bloggs", AccountType.CURRENT_ACCOUNT,
                BigDecimal.valueOf(1250.38));
        BankAccount account2 = new BankAccount("A67890", "Jane Doe", AccountType.SAVINGS_ACCOUNT,
                BigDecimal.valueOf(1550.40));
        BankAccount account3 = new BankAccount("A67891", "Uzumaki Naruto", AccountType.SAVINGS_ACCOUNT,
                BigDecimal.valueOf(1550.40));
        BankAccount account4 = new BankAccount("A67892", "Uchiha Sasuke", AccountType.SAVINGS_ACCOUNT,
                BigDecimal.valueOf(1550.40));
        BankAccount account5 = new BankAccount("A67893", "Sakura Haruno", AccountType.SAVINGS_ACCOUNT,
                BigDecimal.valueOf(1550.40));

        accountCache.put(account1.getAccountId(), account1);
        accountCache.put(account2.getAccountId(), account2);
        accountCache.put(account3.getAccountId(), account3);
        accountCache.put(account4.getAccountId(), account4);
        accountCache.put(account5.getAccountId(), account5);
    }

    /**
     * Add account to cache
     *
     * @param account
     */
    public void createBankAccount(BankAccount account) {

        /* check balance is within allowed limits */
        if (account.getAccountBalance().doubleValue() >= configuration.getMinBalance() &&
                account.getAccountBalance().doubleValue() <= configuration.getMaxBalance()) {

            accountCache.put(account.getAccountId(), account);
        } else {

            throw new InvalidAccountBalanceException("Bank Account Balance is outside of allowed thresholds");
        }
    }

    /**
     * Get account from cache
     *
     * @param account
     */
    public BankAccount retrieveBankAccount(String accountId) {
        return accountCache.get(accountId);
    }
}
