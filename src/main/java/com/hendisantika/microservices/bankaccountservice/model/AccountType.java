package com.hendisantika.microservices.bankaccountservice.model;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.27
 */
public enum AccountType {
    CURRENT_ACCOUNT,
    SAVINGS_ACCOUNT
}
